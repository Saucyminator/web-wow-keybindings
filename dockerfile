FROM ruby:3.0.6-alpine3.16

RUN apk update
RUN apk add --no-cache build-base gcc cmake git

RUN gem update bundler && gem install bundler 'jekyll:4.1.0'
