# Getting Started
A website to display my World of Warcraft character's keybindings by class.

Accessed by the web: https://saucyminator.gitlab.io/web-wow-keybindings/


## Development (locally using Docker)
1. Fork, clone or download this project
1. Download and setup Docker Desktop from [docker.com][]
1. Download and setup VS Code from [code.visualstudio.com][]  
  a. Install both the "Docker" and "Dev Containers" plugins
1. Open the folder with VSCode and a popup should be shown of folder containing a Dev Container configuration file. Choose "**Reopen in Container**"  
  a. ![](src/images/docs/vscode_dev_container_popup.png)
1. Run the following commands:  
  a. ```gem update --system```  
  a. ```bundle install```  
1. Command to access the website: ```bundle exec jekyll serve```  
  a. Open website locally at http://127.0.0.1:4000/web-wow-keybindings/  

Read more at Jekyll's [documentation][] and more information is available at GitLab's [Jekyll pages][].


# Credits
[GitLab Jekyll example][]


[GitLab Jekyll example]: https://gitlab.com/pages/jekyll
[docker.com]: https://www.docker.com/
[code.visualstudio.com]: https://code.visualstudio.com/
[install]: https://jekyllrb.com/docs/installation/
[documentation]: https://jekyllrb.com/docs/home/
[jekyll pages]: https://gitlab.com/pages/jekyll
